package com.nuttek.kuai.kserver.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * 通用查询服务，不需要登录。
 * 
 * @author Frank Yao
 * @date Feb/22/2017
 */

@Service
@Scope("prototype")
public class CommonService {

	public int login(String phone, String vcode) {
		return 1;
	}

}
