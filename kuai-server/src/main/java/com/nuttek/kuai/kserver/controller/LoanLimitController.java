package com.nuttek.kuai.kserver.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.nuttek.kuai.kserver.bean.LoanLimit;
import com.nuttek.kuai.kserver.service.LoanLimitService;

@Controller
public class LoanLimitController {
	@Resource
	private LoanLimitService loanLimitService;

	/**
	 * 查询客服联系方式
	 * 
	 * @return
	 */
	@RequestMapping(value = "/limit", method = { RequestMethod.GET })
	@ResponseBody
	public String getHelp() {
		LoanLimit loanLimit = loanLimitService.getLimitLevel1();
		String json = JSON.toJSONString(loanLimit, true);
		return json;
	}

}
