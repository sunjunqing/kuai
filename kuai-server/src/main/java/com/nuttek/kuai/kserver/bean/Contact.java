package com.nuttek.kuai.kserver.bean;

import org.springframework.data.annotation.Id;

public class Contact {
	@Id
	private String id;
	private String userId;
	private String contactName;
	private String[] contactPhones;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String[] getContactPhones() {
		return contactPhones;
	}

	public void setContactPhones(String[] contactPhones) {
		this.contactPhones = contactPhones;
	}

	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Contact() {
		super();
	}

	public Contact(String userId, String contactName, String[] contactPhones, String orderId) {
		super();
		this.userId = userId;
		this.contactName = contactName;
		this.contactPhones = contactPhones;
		this.orderId = orderId;
	}

}
