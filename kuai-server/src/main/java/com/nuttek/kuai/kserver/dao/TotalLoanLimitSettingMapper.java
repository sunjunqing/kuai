package com.nuttek.kuai.kserver.dao;

import com.nuttek.kuai.kserver.bean.TotalLoanLimitSetting;

public interface TotalLoanLimitSettingMapper {
	TotalLoanLimitSetting select();
}
