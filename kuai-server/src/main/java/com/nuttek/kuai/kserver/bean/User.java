package com.nuttek.kuai.kserver.bean;

public class User {
	private String userId;
	private String userName;
	private String userStatus;
	private String accountId; // 资金账户ID
	private int customerLevel; // 客户等级：0 - 准消费者；1 - 不安全消费者；2 - 安全消费者。
	private int currentPromoPathNo; // 当前升级路径编号
	private String avatarImage; // 头像文件路径

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public int getCustomerLevel() {
		return customerLevel;
	}

	public void setCustomerLevel(int customerLevel) {
		this.customerLevel = customerLevel;
	}

	public int getCurrentPromoPathNo() {
		return currentPromoPathNo;
	}

	public void setCurrentPromoPathNo(int currentPromoPathNo) {
		this.currentPromoPathNo = currentPromoPathNo;
	}

	public String getAvatarImage() {
		return avatarImage;
	}

	public void setAvatarImage(String avatarImage) {
		this.avatarImage = avatarImage;
	}
}
