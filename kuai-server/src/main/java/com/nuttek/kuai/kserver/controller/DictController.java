package com.nuttek.kuai.kserver.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.nuttek.kuai.kserver.dao.BankDictDao;

@Controller
@RequestMapping("/dict")
public class DictController {

	@Autowired
	private BankDictDao bankDictDao;

	/**
	 * TODO 汉字乱码
	 * 
	 * @return
	 */
	@RequestMapping(value = "/bank_dict", method = { RequestMethod.GET })
	@ResponseBody
	public String getBankDict(@RequestHeader(value = "If-Modified-Since", required = false) String lastModified,
			@RequestParam(value = "type") String type,
			@RequestParam(value = "bank_code", required = false) String bankCode,
			@RequestParam(value = "province_code", required = false) String provinceCode,
			@RequestParam(value = "city_code", required = false) String cityCode,
			@RequestParam(value = "district_code", required = false) String districtCode) {
		String reqStr = "{type: 'bank,province', bank_code: '102', province_code: '110000', city_code: '110100', district_code: '110105'}";
		Object req = JSON.parse(reqStr);
		String sampleStr = "{banks: [{bank_code: '102', bank_name: '中国工商银行'}, {bank_code: '103', bank_name: '中国农业银行'}], provinces:[{province_code: '110000', province_name: '北京市'}, {province_code: '120000', province_name: '天津市'}], cities:[{city_code: '110100', city_name: '北京市'}, {city_code: '120100', city_name: '天津市'}, {city_code: '130100', city_name: '石家庄市'}], districts:[{district_code: '110105', district_name: '朝阳区'}, {district_code: '110108', district_name: '海淀区'}], branches:[{branch_code: '102100000345', branch_name: '中国工商银行股份有限公司北京朝阳支行'}, {branch_code: '102100000353', branch_name: '中国工商银行股份有限公司北京望京支行'}, {branch_code: '102100000370', branch_name: '中国工商银行股份有限公司北京广渠路支行'}]}";
		String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		return json;
	}

	/**
	 * 查询银行网点字典
	 * 
	 * @param type
	 *            字典类型 bank,province,city,district
	 * @param province_code
	 *            省code110000
	 * @param city_code
	 *            市code110100
	 * @param district_code
	 *            县110111
	 * @param
	 * @return
	 */
	@RequestMapping(value = "bank_dict", method = { RequestMethod.GET })
	@ResponseBody
	public List[] bank_dict(String type, String province_code, String city_code, String district_code,
			String bank_code) {

		List<Map> bankList = new ArrayList();
		List<Map> provinceList = new ArrayList();
		List<Map> cityList = new ArrayList();
		List<Map> districtList = new ArrayList();
		List<Map> branchList = new ArrayList();
		String[] typeS = type.split(",");
		int length = 0;
		List<List<Map>> l = new ArrayList();
		for (String s : typeS) {
			if ("bank".equals(s) && "".equals(province_code) && "".equals(city_code) && "".equals(district_code)) {
				bankList = bankDictDao.getBankDict();
				l.add(bankList);
			} else if ("province".equals(s) && "".equals(province_code) && "".equals(city_code)
					&& "".equals(district_code)) {
				provinceList = bankDictDao.getProvinceDict();
				l.add(provinceList);
			} else if ("city".equals(s) && province_code != null) {
				province_code = province_code.substring(0, 2) + "%";
				cityList = bankDictDao.getCityDict(province_code);
				l.add(cityList);
			} else if ("district".equals(s) && city_code != null) {
				city_code = city_code.substring(0, 4) + "%";
				districtList = bankDictDao.getDistrictDict(city_code);
				l.add(districtList);
			} else if ("branch".equals(s) && bank_code != null && district_code != null) {
				district_code = district_code.substring(0, 6) + "%";
				branchList = bankDictDao.getBranchDict(bank_code, district_code);
				l.add(branchList);
			}
		}
		List[] result = new List[l.size()];
		int i = 0;
		for (List<Map> ll : l) {
			result[i] = ll;
			i++;
		}
		return result;
	}

}
