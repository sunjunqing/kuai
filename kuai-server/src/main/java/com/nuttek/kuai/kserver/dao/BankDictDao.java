package com.nuttek.kuai.kserver.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface BankDictDao {

	List<Map> getBankDict();

	List<Map> getProvinceDict();

	List<Map> getCityDict(@Param("province_code") String province_code);

	List<Map> getDistrictDict(@Param("city_code") String city_code);

	List<Map> getBranchDict(@Param("bank_code") String bank_code,@Param("district_code")  String district_code);
	
	Map getbankCard(@Param("userId") String userId);

}
