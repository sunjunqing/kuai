package com.nuttek.kuai.kserver.service;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.nuttek.kuai.kserver.bean.SumLoanAmount;
import com.nuttek.kuai.kserver.bean.TotalLoanLimit;
import com.nuttek.kuai.kserver.dao.SumLoanAmountMapper;
import com.nuttek.kuai.kserver.dao.TotalLoanLimitMapper;
import com.nuttek.kuai.kserver.exception.ScareLoanException;

/**
 * 通用查询服务，不需要登录。
 * 
 * @author Frank Yao
 * @date Feb/22/2017
 */

@Service
@Scope("prototype")
public class TotalLoanLimitService {

	private static final int DEFAULT_DAILY_TOTAL_LOAN_LIMIT = 100000;

	@Resource
	private TotalLoanLimitMapper totalLoanLimitMapper;

	@Resource
	private SumLoanAmountMapper sumLoanAmountMapper;

	/**
	 * 实时更新剩余放款额度比例。
	 * 
	 * @return
	 * @throws Exception
	 */
	public String refresh() throws ScareLoanException {
		double prop = calculateProportion();
		NumberFormat format = NumberFormat.getPercentInstance();
		return format.format(prop);
	}

	/**
	 * 计算当前的剩余放款总额度的比例。（百分比%）
	 * 
	 * @return
	 * @throws Exception
	 */
	public double calculateProportion() throws ScareLoanException {
		// 查询当日的放款总额度
		double limit = getTotalLimit();
		System.out.println("total loan limit:" + limit);

		// 查询已放款的总金额
		double sum = sumLoanAmount();
		System.out.println("sum loan amount:" + sum);

		double restLimit = limit - sum;
		if (restLimit < 300 || restLimit < 500) {
			throw new ScareLoanException("Loan sold out");
		}

		// sum / limit = 已成功放款占总额度的比例
		double prop = (1 - sum / limit); // 计算剩余比例。

		return prop;
	}

	/**
	 * 获得当天放款的总额度。<br>
	 * 保存在MySQL中，同步到Redis中，保证所有Server实例读到的数据一致。 <br>
	 * 每天上午8点开放额度。<br>
	 * 事先要更新总额度。再之前要看之前的资金池、放款情况、热度等等。
	 * 
	 * @throws ScareLoanException
	 */
	public double getTotalLimit() throws ScareLoanException {
		// 查询当天开放的总额度，要检查当天的放款时间和时长，只有当天日期和时间大于当天的开放时间，并且小于开放时间+24小时，才能得到当日的额度，否则应处于“尚未开放”状态。
		String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		TotalLoanLimit limit = totalLoanLimitMapper.select(today);
		check(limit);
		// checkTotalLimit(limit);
		return limit.getTotalLoanLimit();
	}

	private double check(TotalLoanLimit limit) throws ScareLoanException {
		if (limit == null) {
			throw new ScareLoanException("Not opened");
		}

		return limit.getTotalLoanLimit();
	}

	/**
	 * 实时汇总当天已成功放款的金额的总和。<br>
	 * TODO 每次放款成功后要更新总金额字段。
	 */
	public double sumLoanAmount() {
		// 已成功的放款总金额
		String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		SumLoanAmount sumLoanAmount = sumLoanAmountMapper.select(today);
		double sum = sumLoanAmount.getSumLoanAmount();

		return sum;
	}

	/**
	 * TODO 改表结构，增加关闭时间（不包含），检查时比较当前时间大于等于开放时间，小于关闭时间。
	 * 
	 * @param limit
	 * @return
	 * @throws ScareLoanException
	 */
	private double checkTotalLimit(TotalLoanLimit limit) throws ScareLoanException {
		// 当前日期和时间
		final long nowMillis = new Date().getTime();

		// 开放时间
		String time = limit.getOpenTime();
		String[] times = time.split(":");
		String hour = times[0];
		String minute = times[1];
		Calendar cal = Calendar.getInstance();
		cal.setTime(limit.getOpenDate());
		cal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hour));
		cal.set(Calendar.MINUTE, Integer.valueOf(minute));
		cal.set(Calendar.SECOND, 0);
		final long openTime = cal.getTimeInMillis();

		// 一个小时的毫秒数
		final long onedayMillis = 60 * 60 * 1000;

		// 24小时的毫秒数
		final long durationMillis = limit.getOpenDuration() * onedayMillis - 1;

		// 结束日期和时间
		final long endTime = openTime + durationMillis;

		System.out.println("now: " + new Date(nowMillis));
		System.out.println("openTime: " + new Date(openTime));
		System.out.println("endTime: " + new Date(endTime));

		if (nowMillis >= openTime && nowMillis <= endTime) {
			// Opened
		} else {
			throw new ScareLoanException("Not opened");
		}

		return limit.getTotalLoanLimit();
	}

	public static void main(String[] args) {
		DateFormat.getDateInstance().getCalendar();
		Calendar cal = Calendar.getInstance();

		String[] times = "20:00".split(":");
		String hour = times[0];
		String minute = times[1];
		cal.set(Calendar.DAY_OF_MONTH, 8);
		cal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hour));
		cal.set(Calendar.MINUTE, Integer.valueOf(minute));
		cal.set(Calendar.SECOND, 0);
		System.out.println(cal.getTime());

		String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		System.out.println(today);
	}
}
