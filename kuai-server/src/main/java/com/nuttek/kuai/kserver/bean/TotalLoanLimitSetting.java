package com.nuttek.kuai.kserver.bean;

import java.util.Date;

public class TotalLoanLimitSetting {
	private static final int DEFAULT_DAILY_TOTAL_LOAN_LIMIT = 100000;

	private int settingId;
	private Date openDate = new Date(117, 2, 8, 20, 0, 0);
	private Date openTime;
	private double totalLoanLimit = DEFAULT_DAILY_TOTAL_LOAN_LIMIT;
	private int openDuration = 24;

	public int getSettingId() {
		return settingId;
	}

	public void setSettingId(int settingId) {
		this.settingId = settingId;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public Date getOpenTime() {
		return openTime;
	}

	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}

	public double getTotalLoanLimit() {
		return totalLoanLimit;
	}

	public void setTotalLoanLimit(double totalLoanLimit) {
		this.totalLoanLimit = totalLoanLimit;
	}

	public int getOpenDuration() {
		return openDuration;
	}

	public void setOpenDuration(int openDuration) {
		this.openDuration = openDuration;
	}

}
