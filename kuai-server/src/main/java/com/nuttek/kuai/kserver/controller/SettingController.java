package com.nuttek.kuai.kserver.controller;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin/setting/")
public class SettingController {
	@Resource
	private RedisTemplate redisTemplate;
	
	@RequestMapping(value = "/total_loan_limit", method = { RequestMethod.GET })
	@ResponseBody
	public String setLoanLimit() {
		String key = "total_loan_limit";
		ValueOperations<String, Object> value = redisTemplate.opsForValue();
		value.set(key, "100000");
		return value.get(key).toString();
	}

}
