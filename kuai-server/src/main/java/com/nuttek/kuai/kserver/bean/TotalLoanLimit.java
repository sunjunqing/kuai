package com.nuttek.kuai.kserver.bean;

import java.util.Date;

public class TotalLoanLimit {

	private Date openDate;
	private String openTime;
	private double totalLoanLimit;
	private int openDuration;

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}

	public double getTotalLoanLimit() {
		return totalLoanLimit;
	}

	public void setTotalLoanLimit(double totalLoanLimit) {
		this.totalLoanLimit = totalLoanLimit;
	}

	public int getOpenDuration() {
		return openDuration;
	}

	public void setOpenDuration(int openDuration) {
		this.openDuration = openDuration;
	}

}
