package com.nuttek.kuai.kserver.service;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.nuttek.kuai.kserver.bean.LoanLimit;
import com.nuttek.kuai.kserver.dao.LoanItemSettingMapper;

@Service
@Scope("prototype")
public class LoanLimitService {
	@Resource
	private LoanItemSettingMapper loanItemSettingMapper;

	/**
	 * 获取全新客户的借款额度及费用说明。
	 * @return
	 */
	public LoanLimit getLimitLevel1() {
		String limitStr = loanItemSettingMapper.select("Loan-L1").getValue();
		String serviceFeeStr = loanItemSettingMapper.select("Service-L1").getValue();
		String periodStr = loanItemSettingMapper.select("LoanPeriod").getValue();
		String interestStr = loanItemSettingMapper.select("Interest").getValue();
		double limit = Double.valueOf(limitStr);
		double serviceFee = Double.valueOf(serviceFeeStr);
		int period = Integer.valueOf(periodStr);
		double interest = Double.valueOf(interestStr);

		LoanLimit loanLimit = new LoanLimit();
		loanLimit.setLoanLimit(limit);
		loanLimit.setLoanPeriod(period);
		loanLimit.setPrincipal(limit);
		loanLimit.setInterest(interest);
		loanLimit.setServiceFee(serviceFee);

		return loanLimit;
	}

	/**
	 * @return
	 */
	public LoanLimit getLimitLevel2(int time) {
		String limitStr = loanItemSettingMapper.select("Loan-L2").getValue();
		String item = "Service-L2-" + time;
		String serviceFeeStr = loanItemSettingMapper.select(item).getValue();
		String periodStr = loanItemSettingMapper.select("LoanPeriod").getValue();
		String interestStr = loanItemSettingMapper.select("Interest").getValue();
		double limit = Double.valueOf(limitStr);
		double serviceFee = Double.valueOf(serviceFeeStr);
		int period = Integer.valueOf(periodStr);
		double interest = Double.valueOf(interestStr);

		LoanLimit loanLimit = new LoanLimit();
		loanLimit.setLoanLimit(limit);
		loanLimit.setLoanPeriod(period);
		loanLimit.setPrincipal(limit);
		loanLimit.setInterest(interest);
		loanLimit.setServiceFee(serviceFee);

		return loanLimit;
	}
}
