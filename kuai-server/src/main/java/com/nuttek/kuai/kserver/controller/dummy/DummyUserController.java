package com.nuttek.kuai.kserver.controller.dummy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping(value = "/dummy/users")
public class DummyUserController {

	/**
	 * 提交借款人的联系人，每次借款时都要上传一次。
	 * 
	 * @param userId
	 * @param contacts
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/contacts", method = { RequestMethod.POST })
	@ResponseBody
	public String saveContacts(@PathVariable("user_id") String userId, @RequestBody String contacts) {
		System.out.println(userId);
		System.out.println(contacts);
		return "OK";
	}

	/**
	 * 取Pandora认证URL地址，type支持运营商（carrier）、学信网（chsi）、外卖平台（takeout）。
	 * 
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/auth_url", method = { RequestMethod.GET })
	@ResponseBody
	public String getAuthUrl(@RequestParam(value = "type") String type) {
		String url = "";
		switch (type) {
		case "chsi":
			url = "http://pandora:8085/auth_chsi";
			break;
		case "carrier":
			url = "http://pandora:8085/auth_carrier";
			break;
		case "takeout":
			url = "http://pandora:8085/auth_takeout";
			break;
		default:
			url = "no_url";
		}
		return url;
	}

	/**
	 * 验证身份证，上传身份证正反面照片，调用接口提取身份证上的信息。
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/id_card", method = { RequestMethod.POST })
	@ResponseBody
	public String authIDCard(@PathVariable("user_id") String userId) {
		return "{err_code: '40105', err_msg: '身份证有误'}";
	}

	/**
	 * 验证面部特征，调用接口提取特征，同时上传身份证照，比对面部特征和身份证中的照片。
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/face_scan", method = { RequestMethod.POST })
	@ResponseBody
	public String authFaceScan(@PathVariable("user_id") String userId) {
		return "{err_code: '40114', err_msg: '暂不符合借款条件'}";
	}

	/**
	 * 绑定银行卡。
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/bank_card", method = { RequestMethod.POST })
	@ResponseBody
	public String bindBankCard(@PathVariable("user_id") String userId) {
		String sampleStr = "{'bank_card_no': '6222-1234-5678-1234-5678', 'account_name': '李某某', 'bank_code': '102', 'branch_code': '102100000345', 'district_code': '110101'}";
		String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		return json;
	}

	/**
	 * 查询已绑定的银行卡。
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/bank_card", method = { RequestMethod.GET })
	@ResponseBody
	public String getBankCard(@PathVariable("user_id") String userId) {
		String sampleStr = "{'bank_card_no': '6222-1234-5678-1234-5678', 'account_name': '李某某', 'bank_code': '102', 'branch_code': '102100000345', 'district_code': '110101'}";
		String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		return json;
	}

	/**
	 * 修改银行卡信息。
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/bank_card", method = { RequestMethod.PUT })
	@ResponseBody
	public String changeBankCard(@PathVariable("user_id") String userId) {
		String sampleStr = "{'bank_card_no': '6222-1234-5678-1234-5678', 'account_name': '李某某', 'bank_code': '102', 'branch_code': '102100000345', 'district_code': '110101'}";
		String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		return json;
	}

	/**
	 * 解除银行卡的绑定。
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/bank_card", method = { RequestMethod.DELETE })
	@ResponseBody
	public String unbindBankCard(@PathVariable("user_id") String userId) {
		return "";
	}

	@RequestMapping(value = "/{user_id}/sign", method = { RequestMethod.POST })
	@ResponseBody
	public String signContract(@PathVariable("user_id") String userId, @RequestParam String email,
			@RequestBody(required = false) String mailbox) {
		String emailStr = "13912345678@139.com";
		return email + ":::::" + mailbox + ":::::" + emailStr;
	}

	/**
	 * 查询所有的借款记录。
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/loans", method = { RequestMethod.GET })
	@ResponseBody
	public String listLoans(@PathVariable("user_id") String userId) {
		String jsonStr = "{loan_date: '2017-04-01', loan_amount: '300.00', loan_succeed: true, auth_status: '2', principal: '300.00', interest: '0.00', service_fee: '49.00', repay_succeed: null, overdue_status: '', repay_date: '2017-04-15', repay_amount: '349.00'}";
		String json = JSON.toJSONString(JSON.parse(jsonStr), true);
		return json;
	}

	/**
	 * 查询用户的通知。
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/notices", method = { RequestMethod.GET })
	@ResponseBody
	public String getNotices(@PathVariable("user_id") String userId) {
		String jsonStr = "[{title: 'Repayment should be executed in 3 days.', content: 'User 13912345678, your loan should be cut payment in 3 days.', unread: true}, {title: 'Repayment should be executed in 1 day.', content: 'User 13912345678, your loan should be cut payment in 1 day.', unread: true}, {title: 'Repayment is overdued today.', content: 'User 13912345678, your loan is overdued at 2017-3-15.', unread: true}]";
		String json = JSON.toJSONString(JSON.parse(jsonStr), true);
		return json;
	}

	/**
	 * 查询用户的借款额度
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/loan_limit", method = {})
	@ResponseBody
	public String getLoanLimit(@PathVariable("user_id") String userId) {
		String jsonStr = "{loan_limit: '300.00', repay_amount: '349.00', principal: '300.00', interest: '0.00', service_fee: '49.00', loan_term: '14'}";
		String json = JSON.toJSONString(JSON.parse(jsonStr), true);
		return json;
	}

	/**
	 * 退出登录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/{user_id}/logout", method = { RequestMethod.GET })
	@ResponseBody
	public String logout() {
		return "Log out";
	}
}
