package com.nuttek.kuai.kserver.bean;

public class UserLoanLogPointer {
	private String userId;
	private int loanLogNo;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getLoanLogNo() {
		return loanLogNo;
	}

	public void setLoanLogNo(int loanLogNo) {
		this.loanLogNo = loanLogNo;
	}
}
