package com.nuttek.kuai.kserver.controller.dummy;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Refer: http://projects.spring.io/spring-data-redis/#quick-start
 * @author Administrator
 *
 */
@Controller
public class RedisController {
	@Resource
	private RedisTemplate redisTemplate;

	/**
	 * 查询客服邮箱地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/redis", method = { RequestMethod.GET })
	@ResponseBody
	public String getRedis() {
		ValueOperations<String, Object> value = redisTemplate.opsForValue();
		value.set("qq", "987654321");
		return value.get("qq").toString();
	}
}
