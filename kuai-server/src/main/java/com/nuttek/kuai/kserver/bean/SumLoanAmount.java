package com.nuttek.kuai.kserver.bean;

import java.util.Date;

public class SumLoanAmount {
	private Date sumDate;
	private double sumLoanAmount;
	private Date sumStartDate;
	private Date sumEndDate;

	public Date getSumDate() {
		return sumDate;
	}

	public void setSumDate(Date sumDate) {
		this.sumDate = sumDate;
	}

	public double getSumLoanAmount() {
		return sumLoanAmount;
	}

	public void setSumLoanAmount(double sumLoanAmount) {
		this.sumLoanAmount = sumLoanAmount;
	}

	public Date getSumStartDate() {
		return sumStartDate;
	}

	public void setSumStartDate(Date sumStartDate) {
		this.sumStartDate = sumStartDate;
	}

	public Date getSumEndDate() {
		return sumEndDate;
	}

	public void setSumEndDate(Date sumEndDate) {
		this.sumEndDate = sumEndDate;
	}

}
