package com.nuttek.kuai.kserver.dao;

import com.nuttek.kuai.kserver.bean.TotalLoanLimit;

public interface TotalLoanLimitMapper {
	TotalLoanLimit select(String today);
}
