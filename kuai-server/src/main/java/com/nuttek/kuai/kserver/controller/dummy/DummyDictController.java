package com.nuttek.kuai.kserver.controller.dummy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping(value = "/dummy")
public class DummyDictController {

	/**
	 * TODO 汉字乱码
	 * 
	 * @return
	 */
	@RequestMapping(value = "/bank_dict", method = { RequestMethod.GET })
	@ResponseBody
	public String getBankDict(@RequestHeader(value = "If-Modified-Since", required = false) String lastModified,
			@RequestParam(value = "type") String type,
			@RequestParam(value = "bank_code", required = false) String bankCode,
			@RequestParam(value = "province_code", required = false) String provinceCode,
			@RequestParam(value = "city_code", required = false) String cityCode,
			@RequestParam(value = "district_code", required = false) String districtCode) {
		String reqStr = "{type: 'bank,province', bank_code: '102', province_code: '110000', city_code: '110100', district_code: '110105'}";
		Object req = JSON.parse(reqStr);
		String sampleStr = "{banks: [{bank_code: '102', bank_name: '中国工商银行'}, {bank_code: '103', bank_name: '中国农业银行'}], provinces:[{province_code: '110000', province_name: '北京市'}, {province_code: '120000', province_name: '天津市'}], cities:[{city_code: '110100', city_name: '北京市'}, {city_code: '120100', city_name: '天津市'}, {city_code: '130100', city_name: '石家庄市'}], districts:[{district_code: '110105', district_name: '朝阳区'}, {district_code: '110108', district_name: '海淀区'}], branches:[{branch_code: '102100000345', branch_name: '中国工商银行股份有限公司北京朝阳支行'}, {branch_code: '102100000353', branch_name: '中国工商银行股份有限公司北京望京支行'}, {branch_code: '102100000370', branch_name: '中国工商银行股份有限公司北京广渠路支行'}]}";
		String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		return json;
	}

}
