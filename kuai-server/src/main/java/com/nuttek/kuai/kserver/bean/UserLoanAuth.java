package com.nuttek.kuai.kserver.bean;

import java.sql.Date;

public class UserLoanAuth {
	private String userId;
	private int loanId;
	private boolean firstLoan;
	private int authStep;
	private boolean authSucceed;
	private Date applyTime;
	private Date succeedTime;
	private Date failureTime;
}
