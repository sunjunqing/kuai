package com.nuttek.kuai.kserver.service;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.nuttek.kuai.kserver.bean.LoanLimit;
import com.nuttek.kuai.kserver.bean.User;
import com.nuttek.kuai.kserver.bean.UserLoanLogPointer;
import com.nuttek.kuai.kserver.bean.UserLoanLogStack;
import com.nuttek.kuai.kserver.bean.UserLoanStatus;
import com.nuttek.kuai.kserver.dao.UserLoanLogPointerMapper;
import com.nuttek.kuai.kserver.dao.UserLoanLogStackMapper;
import com.nuttek.kuai.kserver.dao.UserLoanStatusMapper;
import com.nuttek.kuai.kserver.dao.UserMapper;

/**
 * 核心业务服务。 <br>
 * 借款：首次借款认证 - 通讯录、身份证、学信网、运营商、外卖、人脸、银行卡。 <br>
 * 再次借款认证 - 运营商、外卖、人脸。 <br>
 * 还款：还款，如果是首次借款的还款则在还款成功后自动提升额度。 <br>
 * 查询借款账单。 <br>
 * 银行卡：查询、绑定、解绑银行卡。
 * 
 * @author Frank Yao
 * @date Feb/22/2017
 */
@Service
@Scope("prototype")
public class UserService {
	@Resource
	private UserMapper userMapper;
	@Resource
	private LoanLimitService loanLimitService;
	@Resource
	private UserLoanLogPointerMapper userLoanLogPointerMapper;
	@Resource
	private UserLoanLogStackMapper userLoanLogStackMapper;
	@Resource
	private UserLoanStatusMapper userLoanStatusMapper;

	/**
	 * 获取用户的借款额度
	 * 
	 * @param userId
	 * @return
	 */
	public LoanLimit getUserLoanLimit(String userId) {
		User user = userMapper.selectById(userId);

		LoanLimit loanLimit = null;
		switch (user.getCustomerLevel()) {
		case 0:
			break;
		case 1:
			loanLimit = loanLimitService.getLimitLevel1();
			break;
		case 2:
			UserLoanLogPointer userLoanLogPointer = userLoanLogPointerMapper.select(userId);
			UserLoanLogStack userLoanLogStack = userLoanLogStackMapper.select(userId, userLoanLogPointer.getLoanLogNo());
			int freq = userLoanLogStack.getTimeInPromoPath();
			if (freq > 10) {
				freq = 10;
			}
			loanLimit = loanLimitService.getLimitLevel2(freq);
			break;
		default:
			break;
		}

		return loanLimit;
	}
	
	public UserLoanStatus queryUserLoanStatus(String userId) {
		return userLoanStatusMapper.selectById(userId);
	}

	/**
	 * 测试用
	 * 
	 * @param userId
	 * @return
	 */
	public User getUserInfo(String userId) {
		return userMapper.selectById(userId);
	}
}
