package com.nuttek.kuai.kserver.controller.dummy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping(value = "/dummy")
public class DummyLoginController {

	/**
	 * 发送短信验证码
	 * 
	 * @param phone
	 * @return
	 */
	@RequestMapping(value = "vcode", method = { RequestMethod.GET })
	@ResponseBody
	public String getVcode(@RequestBody String phone) {
		String sampleStr = "{resend_after: 60, valid_in: 1800}";
		String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		return json;

	}

	/**
	 * 登录
	 * 
	 * @param phone
	 * @param vcode
	 * @return
	 */
	@RequestMapping(value = "login", method = { RequestMethod.POST })
	@ResponseBody
	public String login(@RequestBody String phone, @RequestBody String vcode) {
		System.out.println(phone);
		System.out.println(vcode);
		String resultStr = "{access_token: '8B42DFD756397447599813DE402A946B', new_customer: true, unread_notices: true}";
		String json = JSON.toJSONString(JSON.parse(resultStr), true);
		return json;
	}

}
