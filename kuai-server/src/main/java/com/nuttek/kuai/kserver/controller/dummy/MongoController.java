package com.nuttek.kuai.kserver.controller.dummy;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.nuttek.kuai.kserver.bean.Contact;

@Controller
@RequestMapping(value = "/dummy")
public class MongoController {
	@Autowired
	protected MongoTemplate mongoTemplate;

	@RequestMapping(value = "/getMongo")
	@ResponseBody
	public String getMongo() {
		Query query = new Query();
		List<Contact> list = mongoTemplate.find(query, Contact.class);
		return JSON.toJSONString(list, true);
	}

	@RequestMapping(value = "/saveMongo")
	@ResponseBody
	public String saveMongo() {
		Contact contact = new Contact();
		contact.setUserId("13912345678");
		contact.setContactName("刘某某");
		contact.setContactPhones(new String[] { "022-12345678", "13012345678" });
		mongoTemplate.save(contact);
		return "OK";
	}
}
