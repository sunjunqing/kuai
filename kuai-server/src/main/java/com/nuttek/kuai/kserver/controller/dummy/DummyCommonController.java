package com.nuttek.kuai.kserver.controller.dummy;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping(value = "/dummy")
public class DummyCommonController {
	@Resource
	private RedisTemplate redisTemplate;

	/**
	 * 查询客服邮箱地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/help", method = { RequestMethod.GET })
	@ResponseBody
	public String getHelp() {
		String key = "cs";
		String sampleStr = "{cs: {email: 'kefu@300kuai.com', telephone: '022-12345678', mobile_phone: '13912345678', qq: '987654321', weixin: 'wx13912345678', weibo: 'weibo13912345678'}}";
		// String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		ValueOperations<String, Object> value = redisTemplate.opsForValue();
		value.set(key, sampleStr);
		String jsonStr = value.get(key).toString();
		String json = JSON.toJSONString(JSON.parse(jsonStr), true);
		return json;
	}

}
