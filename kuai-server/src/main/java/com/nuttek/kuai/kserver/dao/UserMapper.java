package com.nuttek.kuai.kserver.dao;

import com.nuttek.kuai.kserver.bean.User;

public interface UserMapper {
	User selectById(String userId);
}
