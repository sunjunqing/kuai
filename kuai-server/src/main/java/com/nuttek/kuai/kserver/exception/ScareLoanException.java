package com.nuttek.kuai.kserver.exception;

public class ScareLoanException extends Exception {

	public ScareLoanException(String reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -878824487466029950L;

}
