package com.nuttek.kuai.kserver.bean;

import java.sql.Date;

public class UserRepayJournal {
	private String userId;
	private int loanId;
	private int repayJournalId;
	private String repayStatus;
	private String repayFailReason;
	private Date repayDate;
	private float repayAmount;
	private float repayPrincipal;
	private float repayInterest;
	private Date actualRepayTime;
	private float actualRepayAmount;
	private String overdueStatus;
	private int overdueDays;
	private float overdueFee;
	private float overdueRate;
}
