package com.nuttek.kuai.kserver.bean;

public class UserIDCard {
	private String userId;
	private String idCode;
	private String citizenName;
	private String idTerm;
	private String issuingAuthority;
	private String idAddress;
	private String sex;
	private String race;
	private String birthdate;
}
