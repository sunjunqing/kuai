package com.nuttek.kuai.kserver.bean;

public class LoanLimit {
	private double loanLimit;
	private int LoanPeriod;
	private double principal;
	private double interest;
	private double serviceFee;

	public double getLoanLimit() {
		return loanLimit;
	}

	public void setLoanLimit(double loanLimit) {
		this.loanLimit = loanLimit;
	}

	public int getLoanPeriod() {
		return LoanPeriod;
	}

	public void setLoanPeriod(int loanPeriod) {
		LoanPeriod = loanPeriod;
	}

	public double getPrincipal() {
		return principal;
	}

	public void setPrincipal(double principal) {
		this.principal = principal;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public double getServiceFee() {
		return serviceFee;
	}

	public void setServiceFee(double serviceFee) {
		this.serviceFee = serviceFee;
	}

}
