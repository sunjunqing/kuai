package com.nuttek.kuai.kserver.bean;

import java.sql.Date;

public class UserLoan {
	private String loadId;
	private boolean loanSucceed;
	private float loanLimit;
	private float loanAmount;
	private Date loanTime;
	private Date repayDate;
	private float repayAmount;
	private float repayPrincipan;
	private float repayInterest;
	private boolean repaySucceed;
	private Date actualRepayDate;
	private float actualRepayAmount;
	private boolean overdue;
	private int overdueDays;
	private float overdueFee;
	private float overdueRate;

}
