package com.nuttek.kuai.kserver.exception;

public class LoginException extends Exception {

	public LoginException(String reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -878824487466029950L;

}
