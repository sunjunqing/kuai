package com.nuttek.kuai.kserver.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.nuttek.kuai.kserver.bean.UserLoanStatus;
import com.nuttek.kuai.kserver.exception.LoginException;
import com.nuttek.kuai.kserver.service.UserService;
import com.nuttek.kuai.kserver.util.Base64;

@Controller
public class LoginController {
	@Resource
	private UserService userService;

	/**
	 * 发送登录验证码
	 * 
	 * @param phone
	 * @return
	 */
	@RequestMapping(value = "vcode", method = { RequestMethod.GET })
	@ResponseBody
	public String getVcode(@RequestBody String phone) {
		String sampleStr = "{resend_after: 60, valid_in: 1800}";
		String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		return json;
	}

	/**
	 * 登录<br>
	 * 发出登录请求的用户端有如下可能性：一、未注册的游客；二、已注册、已退出的用户；三、已注册、已登录的用户。<br>
	 * 用户端换手机，用户端删除App后重装。用户端换手机（即换用户）。<br>
	 * 验证VCODE<br>
	 * 建立Session<br>
	 * 自动注册 检查设备信息，保存设备信息 生成并返回access token<br>
	 * 检查还款日期并生成提醒<br>
	 * 检查并返回未读的提醒
	 * 
	 * @param phone
	 * @param vcode
	 * @return
	 * @throws LoginException
	 */
	@RequestMapping(value = "login", method = { RequestMethod.GET })
	@ResponseBody
	public String login(@RequestBody String loginStr,
			@RequestHeader(value = "Authorization", required = false) String accessToken) throws LoginException {

		// 验证phone&vcode。（非登录状态）
		String[] login = Base64.decode(loginStr).split(":");
		String userId = login[0];
		String vcode = login[1];
		String vcodeInRedis = getVcodeFromRedis(userId);
		if (vcode.equals(vcodeInRedis)) {
			// 验证码正确。
			String validTime = "2017-03-12 12:14:00";
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mi:ss");
			try {
				Date start = sdf.parse(validTime);
				long valid = start.getTime() + 60 * 1000;
				if (now.getTime() > valid) {
					throw new LoginException("VCode Expired");
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			// 验证码不正确。
		}

		// 验证access_token。（登录状态）
		if (accessToken == null) {
			// 未登录的用户
		} else {
			// 已登录的用户
			String decoded = Base64.decode(accessToken);
			String[] str = decoded.split(":");
			String user = str[0];
			String device = str[1];
			String token = str[2];
			String accessTokenInRedis = getAccessTokenFromRedis(user);
			if (accessTokenInRedis != null) {
				String decodedInRedis = Base64.decode(accessTokenInRedis);
				String strInRedis[] = decodedInRedis.split(":");
				String userInRedis = strInRedis[0];
				String deviceInRedis = strInRedis[1];
				String tokenInRedis = strInRedis[2];
				if (device.equals(deviceInRedis)) {
					// Pass
				} else {
					// Invalidate Access Token In Redis.

				}
			}
		}
		String token = "";
		if (accessToken == null) {
			String uuid = UUID.randomUUID().toString();
			System.out.println(uuid);
			token = Base64.encode(uuid);
			// TODO save token in Redis.
		} else {
			System.out.println(accessToken.substring(0, 6));
			String head = "Basic ";
			accessToken = head + token;
			System.out.println(accessToken);
			String method = accessToken.substring(0, 6);
			if (head.equals(method)) {

			} else {
				// TODO Head not match.
			}
			token = accessToken.substring(6);
			String tokenServer = "";
			if (tokenServer != null && token != null && tokenServer.equals(token)) {
				// TODO token一致，登录成功。
			} else {
				// TODO token不一致，登录失败。
			}
		}

		// 检查用户借款和还款流程及状态。（登录状态）
		UserLoanStatus userLoanStatus = userService.queryUserLoanStatus(userId);
		if (userLoanStatus.getPhase().equals("还款阶段")) {
			System.out.println("");
		}

		// 生成用户还款、逾期等通知信息。（登录状态）
		String repayNotice = "";

		// 检查并打包所有未读的通知消息。（登录状态）
		String unreadNotices = "[{id: 1, title: '注册成功'}, {id: 2, title: '审核通过'}, {id:3, title: '放款成功'}, {id: 4, title: '3天内还款'}, {id: 5, title: '1天内还款'}]";

		// 检查并获取最新的剩余总额度比例。（非登录状态）

		// 检查用户之前借款频次，并获取个人借款说明，如果用户未登录或非注册用户则按新客户处理。（登录或非登录状态）

		// 打包JSON：Token、总额度、个人额度、流程状态、通知等。

		return token;
	}

	private UserLoanStatus queryUserLoanStatus(String userId) {
		UserLoanStatus userLoanStatus = new UserLoanStatus();
		userLoanStatus.setUserId(userId);
		userLoanStatus.setPhase("还款阶段");
		userLoanStatus.setStep("正常还款");
		userLoanStatus.setStatus("未还款");
		return userLoanStatus;
	}

	private String getVcodeFromRedis(String userId) {
		return "REDIS_VCODE: 123456, '2017-03-12 12:14:00'";
	}

	private String getAccessTokenFromRedis(String userId) {
		return "REDIS_AccessToken: ";
	}

	public static void main(String[] args) {
		String uuid = UUID.randomUUID().toString();
		String user = "13912345678"; // Phone
		String vcode = "123456"; // VCODE
		String deviceId = "abcd-123456-1234-12345678";
		// case-1: 未注册的用户，登录时发送Base64(phone,vcode,device)
		String case1 = Base64.encode(user + ":" + vcode + ":" + deviceId);
		System.out.println("case1: " + case1);
		// case-2: 已注册已退出的的用户，登录时发送Base64(phone,vcode,device)
		String case2 = Base64.encode(user + ":" + vcode + ":" + deviceId);
		System.out.println("case2: " + case2);
		// case-3: 已注册未退出的用户，登录时发送access_token
		String access_token = Base64.encode(user + ":" + deviceId + ":" + uuid);
		String case3 = access_token;
		System.out.println("case3: " + case3);
	}
}
