package com.nuttek.kuai.kserver.dao;

import com.nuttek.kuai.kserver.bean.UserLoanStatus;

public interface UserLoanStatusMapper {
	UserLoanStatus selectById(String userId);
}
