package com.nuttek.kuai.kserver.dao;

import com.nuttek.kuai.kserver.bean.SumLoanAmount;

public interface SumLoanAmountMapper {
	SumLoanAmount select(String today);
}
