package com.nuttek.kuai.kserver.bean;

import java.util.Date;

public class UserLoanLogStack {
	private String userId;
	private String loanLogNo;
	private int promoPathNo;
	private int timeInPromoPath;
	private Date loanDate;
	private boolean loaned;
	private boolean repayed;
	private boolean overdued;
	private double overdueFine;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLoanLogNo() {
		return loanLogNo;
	}

	public void setLoanLogNo(String loanLogNo) {
		this.loanLogNo = loanLogNo;
	}

	public Date getLoanDate() {
		return loanDate;
	}

	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}

	public int getPromoPathNo() {
		return promoPathNo;
	}

	public void setPromoPathNo(int promoPathNo) {
		this.promoPathNo = promoPathNo;
	}

	public int getTimeInPromoPath() {
		return timeInPromoPath;
	}

	public void setTimeInPromoPath(int timeInPromoPath) {
		this.timeInPromoPath = timeInPromoPath;
	}

	public boolean isLoaned() {
		return loaned;
	}

	public void setLoaned(boolean loaned) {
		this.loaned = loaned;
	}

	public boolean isRepayed() {
		return repayed;
	}

	public void setRepayed(boolean repayed) {
		this.repayed = repayed;
	}

	public boolean isOverdued() {
		return overdued;
	}

	public void setOverdued(boolean overdued) {
		this.overdued = overdued;
	}

	public double getOverdueFine() {
		return overdueFine;
	}

	public void setOverdueFine(double overdueFine) {
		this.overdueFine = overdueFine;
	}
}
