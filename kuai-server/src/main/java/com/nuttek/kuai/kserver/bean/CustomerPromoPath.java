package com.nuttek.kuai.kserver.bean;

public class CustomerPromoPath {
	private String userId; // 用户ID
	private int customerLevel; // 客户等级：0 - 准消费者；1 - 不安全消费者；2 - 安全消费者。
	private boolean registered; // 是否已注册
	private boolean loaned; // 是否有借款
	private boolean payedOff; // 是否还清了借款
	private boolean overdued; // 是否逾期
	private double overdueFine; // 逾期滞纳金：逾期前两天不产生滞纳金，从第三天开始产生。
	private int totalTimes; // 借款总次数
	private int currentPromoPathNo; // 当前升级路径编号
	private int timesInPromoPath; // 当前升级路径中借款数合计
	private int timeInPromoPath; // 当前升级路径中第几次借款

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getCustomerLevel() {
		return customerLevel;
	}

	public void setCustomerLevel(int customerLevel) {
		this.customerLevel = customerLevel;
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	public boolean isLoaned() {
		return loaned;
	}

	public void setLoaned(boolean loaned) {
		this.loaned = loaned;
	}

	public boolean isPayedOff() {
		return payedOff;
	}

	public void setPayedOff(boolean payedOff) {
		this.payedOff = payedOff;
	}

	public boolean isOverdued() {
		return overdued;
	}

	public void setOverdued(boolean overdued) {
		this.overdued = overdued;
	}

	public double getOverdueFine() {
		return overdueFine;
	}

	public void setOverdueFine(double overdueFine) {
		this.overdueFine = overdueFine;
	}

	public int getTotalTimes() {
		return totalTimes;
	}

	public void setTotalTimes(int totalTimes) {
		this.totalTimes = totalTimes;
	}

	public int getCurrentPromoPathNo() {
		return currentPromoPathNo;
	}

	public void setCurrentPromoPathNo(int currentPromoPathNo) {
		this.currentPromoPathNo = currentPromoPathNo;
	}

	public int getTimesInPromoPath() {
		return timesInPromoPath;
	}

	public void setTimesInPromoPath(int timesInPromoPath) {
		this.timesInPromoPath = timesInPromoPath;
	}

	public int getTimeInPromoPath() {
		return timeInPromoPath;
	}

	public void setTimeInPromoPath(int timeInPromoPath) {
		this.timeInPromoPath = timeInPromoPath;
	}
}
