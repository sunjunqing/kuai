package com.nuttek.kuai.kserver.dao;

import com.nuttek.kuai.kserver.bean.UserLoanLogStack;

public interface UserLoanLogStackMapper {
	UserLoanLogStack select(String userId, int loanLogNo);
}
