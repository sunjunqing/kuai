package com.nuttek.kuai.kserver.controller.dummy;

import java.text.NumberFormat;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/dummy")
public class DummyScareLoanController {
	@Resource
	private RedisTemplate redisTemplate;

	/**
	 * 每天更新一次放款总额度，要可配置。 汇总已成功放款的金额。 剩余放款总额度%=(1 - (当天放款总额度 - 已成功放款的总额)) * 100
	 * 取整数位。
	 * 
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/scare_loan", method = { RequestMethod.GET })
	@ResponseBody
	public String getLoanLimit(HttpServletResponse response) {
		String key = "total_loan_limit";
		ValueOperations<String, Object> value = redisTemplate.opsForValue();
		String totalStr = value.get(key).toString();
		double loaned = 300.00 * 30;
		double total = Double.valueOf(totalStr);
		double prop = 1 - (loaned / total);
		String propStr = NumberFormat.getPercentInstance().format(prop);
		return propStr;
	}

}
