package com.nuttek.kuai.kserver.dao;

import com.nuttek.kuai.kserver.bean.LoanItemSetting;

public interface LoanItemSettingMapper {
	LoanItemSetting select(String item);
}
