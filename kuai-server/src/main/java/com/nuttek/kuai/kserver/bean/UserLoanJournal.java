package com.nuttek.kuai.kserver.bean;

import java.sql.Date;

public class UserLoanJournal {
	private String userId;
	private int loanId;
	private int loanJournalId;
	private String loanStatus;
	private String loanFailReason;
	private float loanLimit;
	private float loanAmount;
	private Date loanTime;
	private Date repayDate;
	private float repayAmount;
	private float repayPrincipal;
	private float repayInterest;
}
