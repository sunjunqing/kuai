package com.nuttek.kuai.kserver.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class LoanService {

	/**
	 * 通讯录认证
	 */
	public void authAddressBook() {
	}

	/**
	 * 身份证认证<br>
	 * TODO 需要学信网接口。
	 */
	public void authIDCard() {
	}

	/**
	 * 学信网认证<br>
	 * TODO 需要学信网接口
	 */
	public void authCHSI() {
	}

	/**
	 * 运营商认证<br>
	 * TODO 需要运营商接口。
	 */
	public void authCarrier() {
	}

	/**
	 * 外卖平台认证<br>
	 * TODO 需要外卖平台接口。
	 */
	public void authOutFood() {
	}

	/**
	 * 人脸识别认证<br>
	 * TODO 需要人脸识别接口。
	 */
	public void authFaceRecog() {
	}

	/**
	 * 银行卡认证<br>
	 * TODO 需要银行接口
	 */
	public void authBankCard() {
		loan();
	}

	/**
	 * 借款发放<br>
	 * TODO 需要银行接口
	 */
	private void loan() {
	}

	/**
	 * 查询未还款的借款
	 */
	public void getLoan() {
	}

	/**
	 * 还款，成功后如果是首次借款的还款则自动提升额度。<br>
	 * TODO 需要银行接口
	 */
	public void repay() {
		raiseLoanLimit();
	}

	/**
	 * 提升额度
	 */
	private void raiseLoanLimit() {
	}

	/**
	 * 查询所有生效的绑定的银行卡
	 */
	public void listBankCard() {
	}

	/**
	 * 解除银行卡的绑定<br>
	 * TODO 需要银行接口
	 */
	public void unbindBankCard() {
	}

	/**
	 * 查询所有借款。
	 */
	public void listLoans() {
	}


}
