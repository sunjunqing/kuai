package com.nuttek.kuai.kserver.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.nuttek.kuai.kserver.bean.Contact;
import com.nuttek.kuai.kserver.dao.AmountDao;
import com.nuttek.kuai.kserver.dao.BankDictDao;
import com.nuttek.kuai.kserver.dao.NoticeDao;

@Controller
@RequestMapping("/common")
public class CommonController {

	@Resource
	private RedisTemplate redisTemplate;

	@Autowired
	protected MongoTemplate mongoTemplate;

	@Resource
	private NoticeDao noticeDao;

	@Resource
	private AmountDao amountDao;

	@Autowired
	private BankDictDao bankDictDao;

	/**
	 * 查询客服联系方式
	 * 
	 * @return
	 */
	@RequestMapping(value = "/help", method = { RequestMethod.GET })
	@ResponseBody
	public String getHelp() {
		String key = "cs";
		String sampleStr = "{cs: {email: 'kefu@300kuai.com', telephone: '022-12345678', mobile_phone: '13912345678', qq: '987654321', weixin: 'wx13912345678', weibo: 'weibo13912345678'}}";
		// String json = JSON.toJSONString(JSON.parse(sampleStr), true);
		ValueOperations<String, Object> value = redisTemplate.opsForValue();
		value.set(key, sampleStr);
		String jsonStr = value.get(key).toString();
		String json = JSON.toJSONString(JSON.parse(jsonStr), true);
		return json;
	}

	/**
	 * 查询客服邮箱地址
	 * 
	 * @param phone电话
	 * @return
	 */
	@RequestMapping(value = "vcode", method = { RequestMethod.GET })
	@ResponseBody
	public String getVcode(@RequestBody String phone) {
		// public String getVcode() {
		// String phone = "18522160192";
		String msg = createRandom(6);
		String urlS = "http://222.73.117.158/msg/HttpBatchSendSM?account=zhilianyy&pswd=Tch609794&mobile=" + phone
				+ "&msg=" + msg;
		String result = "";
		try {
			// 根据地址获取请求
			HttpGet request = new HttpGet(urlS);// 这里发送get请求
			// 获取当前客户端对象
			HttpClient httpClient = new DefaultHttpClient();
			// 通过请求对象获取响应对象
			HttpResponse response = httpClient.execute(request);
			// 判断网络连接状态码是否正常(0--200都数正常)
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				result = EntityUtils.toString(response.getEntity(), "utf-8");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public String createRandom(int length) {
		String retStr = "";
		String strTable = "1234567890";

		int len = strTable.length();
		boolean bDone = true;
		do {
			retStr = "";
			int count = 0;
			for (int i = 0; i < length; i++) {
				double dblR = Math.random() * len;
				int intR = (int) Math.floor(dblR);
				char c = strTable.charAt(intR);
				if (('0' <= c) && (c <= '9')) {
					count++;
				}
				retStr += strTable.charAt(intR);
			}
			if (count >= 2) {
				bDone = false;
			}
		} while (bDone);
		return retStr;
	}

	/**
	 * 提交通讯录
	 * 
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "contacts", method = { RequestMethod.GET })
	@ResponseBody
	public String contacts() {
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
		String order_id = createRandom(4) + sdf.format(new Date());
		mongoTemplate.createCollection(order_id);
		String user_id = "555";
		String contact_name = "ly";
		String[] contact_phone = { "444", "222", "333" };
		Contact contact = new Contact(user_id, contact_name, contact_phone, order_id);
		mongoTemplate.save(contact, order_id);
		return order_id;
	}

	/**
	 * 查询通知 通过mysql查询出所有的通知，再根据mongodb的状态针对通知进行过滤， 实体类是假的
	 * 
	 * @param number是页数从1开始
	 * @return
	 */
	@RequestMapping(value = "notice", method = { RequestMethod.GET })
	@ResponseBody
	public List<Map> notice(int number) {
		int beginNumber = number * 10;
		List<Map> noticeList = noticeDao.getNumNotice(beginNumber);
		Query query = new Query();
		query.addCriteria(Criteria.where("readStatus").is(true));
		Update update = new Update();
		update.set("readStatus", false);
		mongoTemplate.upsert(query, update, "notice");

		return noticeList;

	}

	/**
	 * 查询借款账单 实体类是假的
	 * 
	 * @param number
	 *            页数
	 * @return
	 */
	@RequestMapping(value = "account", method = { RequestMethod.GET })
	@ResponseBody
	public List<Map> account(int number) {

		int beginNumber = number * 10;
		List<Map> accountList = amountDao.getNumAmount(beginNumber);
		return accountList;

	}

	/**
	 * 查询银行卡 实体类是假的
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "bankCard", method = { RequestMethod.GET })
	@ResponseBody
	public Map bankCard(String userId) {

		Map bankCard = bankDictDao.getbankCard(userId);
		return bankCard;

	}

}
