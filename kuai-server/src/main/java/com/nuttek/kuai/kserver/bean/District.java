package com.nuttek.kuai.kserver.bean;

public class District {
	private String districtCode;
	private String distrcitName;
	private String areaCode;

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getDistrcitName() {
		return distrcitName;
	}

	public void setDistrcitName(String distrcitName) {
		this.distrcitName = distrcitName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

}
