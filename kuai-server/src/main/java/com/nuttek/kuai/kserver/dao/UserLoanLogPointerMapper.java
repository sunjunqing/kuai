package com.nuttek.kuai.kserver.dao;

import com.nuttek.kuai.kserver.bean.UserLoanLogPointer;

public interface UserLoanLogPointerMapper {
	UserLoanLogPointer select(String user);
}
